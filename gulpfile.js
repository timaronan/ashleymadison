// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
// var compass = require('gulp-compass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var watch = require('gulp-watch');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

gulp.task('htmlmin', function() {
  return gulp.src('local/index.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('live'));
});

gulp.task('images', function () {
    return gulp.src('local/img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('live/img'));
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('local/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
// gulp.task('compass', function() {
//   gulp.src('local/sass/*.scss')
//     .pipe(compass({
//       config_file: 'local/config.rb',
//       css: 'local/stylesheets',
//       sass: 'local/sass'
//     }))
//     .pipe(gulp.dest('local/stylesheets'));
// });

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('local/js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('local/scripts'))
        .pipe(uglify())
        .pipe(gulp.dest('live/js'));
});

// Concatenate & Minify JS
gulp.task('depends', function() {
    return gulp.src('local/dist/*.js')
        .pipe(concat('depends.js'))
        .pipe(gulp.dest('local/scripts'))
        .pipe(uglify())    
        .pipe(gulp.dest('live/scripts'));
});

gulp.task('minify-css', function() {
  return gulp.src('local/stylesheets/*.css')
    .pipe(minifyCSS({keepBreaks:true}))
    .pipe(gulp.dest('live/stylesheets'));
});

gulp.task('data', function() {
  return gulp.src('local/build/*.json')
    .pipe(gulp.dest('live/build'));
});

gulp.task('database', function() {
  return gulp.src('local/*.json')
    .pipe(gulp.dest('live'));
});

gulp.task('fonts', function() {
  return gulp.src('local/fonts/*')
    .pipe(gulp.dest('live/fonts'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('local/js/*.js', ['lint', 'scripts']);
    gulp.watch('local/dist/*.js', ['depends']);
    gulp.watch('local/img/*', ['images']);
    // gulp.watch('local/sass/*.scss', ['compass']);
    gulp.watch('local/stylesheets/*.css', ['minify-css']);
    gulp.watch('local/*.html', ['htmlmin']);
    gulp.watch('local/*.json', ['data']);
    gulp.watch('local/*.json', ['database']);
});

// Default Task
// gulp.task('default', ['data', 'fonts', 'images', 'lint', 'depends', 'scripts', 'compass', 'minify-css', 'htmlmin', 'watch']);
gulp.task('default', ['data', 'fonts', 'images', 'lint', 'depends', 'scripts', 'minify-css', 'htmlmin', 'watch', 'database']);