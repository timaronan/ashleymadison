local/build/gz_2010_us_050_00_20m.zip:
	mkdir -p $(dir $@)
	curl -o $@ http://www2.census.gov/geo/tiger/GENZ2010/$(notdir $@)

local/build/gz_2010_us_050_00_20m.shp: local/build/gz_2010_us_050_00_20m.zip
	unzip -od $(dir $@) $<
	touch $@

local/build/counties.json: local/build/gz_2010_us_050_00_20m.shp local/build/ashley.csv
	node_modules/.bin/topojson \
		-o $@ \
		--id-property='STATE+COUNTY,st' \
		--external-properties=local/build/ashley.csv \
		--properties='trx=+d.properties["trx"]' \
		--properties='county=d.properties["county"]' \
		--properties='state=d.properties["state"]' \
		--properties='spend=+d.properties["spend"]' \
		--properties='persons=+d.properties["persons"]' \
		--properties='trx_male=+d.properties["trx_male"]' \
		--properties='count_male=+d.properties["count_male"]' \
		--properties='spend_male=+d.properties["spend_male"]' \
		--properties='trx_female=+d.properties["trx_female"]' \
		--properties='count_female=+d.properties["count_female"]' \
		--properties='spend_female=+d.properties["spend_female"]' \
		--projection='width = 960, height = 600, d3.geo.albersUsa() \
			.scale(1280) \
			.translate([width / 2, height / 2])' \
		--simplify=.5 \
		-- counties=$<

local/build/states.json: local/build/counties.json
	node_modules/.bin/topojson-merge \
		-o $@ \
		--in-object=counties \
		--out-object=states \
		--key='d.id.substring(0, 2)' \
		-- $<

local/us.json: local/build/states.json
	node_modules/.bin/topojson-merge \
		-o $@ \
		--in-object=states \
		--out-object=nation \
		-- $<