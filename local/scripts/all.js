var width = 1400,
    height = 1100,
    centered;

var path = d3.geo.path()
    .projection(null);

var radius = d3.scale.sqrt()
    .range([0, 50]);

var svg = d3.select(".map").append("svg")
    .attr("width", "100%")
    .attr("height", "100%")
    .attr("viewBox", "-300 -300 "+width+" "+height);
var dom = 'mdom';
var unr = 0;
var curSwipe = 0;
var x,y,sy;
var spntxt = 'Money Spent';
var memtxt = 'Paying Members';
var imgs = {
  'female' : '<svg width="90%" height="90%" viewBox="0 0 37 62" enable-background="new 0 0 37 62" xml:space="preserve"><path fill="none" d="M10.4,13C0.3-2,34.4-5.1,27.8,13.4"/><path fill="none" d="M5,44.9c0,0-3.1-7.6-1.6-9.3c-1-0.3-1.9-6-1.9-6  c-1.2-11.1,7.2-15.8,8-16.2c10.9-5.3,18.2,0.5,18.2,0.5s8.9,5.7,8.2,13.7c0,0,0.1,5.6-1.4,8.1c-1.1,2.2,3.2,0,0,7.3  C31.4,50.2,24.1,59,21.1,59.2s-3.6,0.5-4.6,0C13.8,59.7,5,44.9,5,44.9z"/><path fill="none" d="M4.8,60.5c0,0-2-4.5-2.5-8.2s-0.5-6.6-0.5-9.2c0-2.6,0.9-5.3,1.6-6.6  c0.7-1.3,1.4-2.9,3.1-3.8c1.8-1,16.2-8.1,17.2-9.5c1-1.4,2.2-5,2.2-5s-0.8,4.2-1.7,5.4c1.3,3.2,2.4,3.9,3.3,4.8  c0.9,0.9,2,1.9,3.4,3.4c1.3,1.5,2.5,2.1,3,3.5s1,0.7,1,3s-0.2,4.6-0.2,4.6s0.2,2.3,0.2,3.7c0,1.4,0,2.6,0,3.9s-0.6,3.1-1.4,4.9  c-0.9,1.8-1.1,2.3-1.1,2.3"/><line fill="none" x1="17.5" y1="37.4" x2="17.5" y2="44.1"/><path fill="none" d="M16.9,45.8c0,0-1,0.7,0,1.3c1,0.6,2,0.5,2.9,0c0.9-0.5,0-1.3,0-1.3"/><path fill="none" d="M14.8,51.8c0,0,0.5-0.7,1.6-1.5c1.1-0.8,2.8-0.6,3.7,0  c1,0.6,1.7,1.5,1.7,1.5"/><path fill="none" d="M17.2,51.6c0,0-0.8,0.5,0,1s1.5,0.4,2.1,0c0.6-0.4,0-1,0-1"/><circle cx="11.6" cy="38.4" r="1"/><path fill="none" d="M8.2,38.4c0,0,3.7-3,6.2,0"/><circle cx="25.8" cy="38.4" r="1"/><path fill="none" d="M29.2,38.4c0,0-3.7-3-6.2,0"/><path fill="none" d="M7.6,34.5c0,0,3.2-4.5,8.3,0.8"/><path fill="none" d="M29.5,34.5c0,0-3.2-4.5-8.3,0.8"/><line fill="none" x1="5.8" y1="33.2" x2="5.6" y2="45.8"/><line fill="none" x1="31.9" y1="33" x2="32.1" y2="47.8"/><path fill="none" d="M3.4,36.6c0,0-0.1,1.8,2.2,2.2"/><path fill="none" d="M31.9,36.6c0,0,1.5,3.1,3.5,2.2"/><rect x="15.9" y="49.8" width="4.7" height="1.5"/></svg>',
  'male' : '<svg width="90%" height="90%" viewBox="0 0 37 62" enable-background="new 0 0 37 62" xml:space="preserve">  <path fill="none" d="M27.8,13.4"/><path fill="none" d="M10.4,13"/><path fill="none" d="M5,44.9c0,0-3.1-7.6-1.6-9.3c-1-0.3-1.9-6-1.9-6  c-1.2-11.1,7.2-15.8,8-16.2c10.9-5.3,18.2,0.5,18.2,0.5s8.9,5.7,8.2,13.7c0,0,0.1,5.6-1.4,8.1c-1.1,2.2,3.2,0,0,7.3  C31.4,50.2,24.1,59,21.1,59.2s-4.6,0-4.6,0C14.2,59.3,5,44.9,5,44.9z"/><path fill="none" d="M3.2,37.6c0.7-1.3,1.7-3.9,3.4-4.8c1.8-1,16.2-8.1,17.2-9.5  c1-1.4,2.2-5,2.2-5s-0.8,4.2-1.7,5.4c1.3,3.2,2.4,3.9,3.3,4.8c0.9,0.9,2,1.9,3.4,3.4c1.3,1.5,2.5,2.1,3,3.5s1.8,3,1.8,3"/><line fill="none" x1="17.5" y1="37.4" x2="17.5" y2="44.1"/><path fill="none" d="M16.9,45.8c0,0-1,0.7,0,1.3c1,0.6,2,0.5,2.9,0c0.9-0.5,0-1.3,0-1.3"/><path fill="none" d="M14.8,52.8c0,0,0.5,0.3,1.6-0.5c1.1-0.8,2.8-0.6,3.7,0  c1,0.6,1.7,0.5,1.7,0.5"/><circle cx="11.6" cy="38.4" r="1"/><path fill="none" d="M8.2,38.4c0,0,3.7-3,6.2,0"/><circle cx="25.8" cy="38.4" r="1"/><path fill="none" d="M29.2,38.4c0,0-3.7-3-6.2,0"/><path fill="none" d="M7.6,34.5c0,0,3.2-4.5,8.3,0.8"/><path fill="none" d="M29.5,34.5c0,0-3.2-4.5-8.3,0.8"/><line fill="none" x1="5.8" y1="33.2" x2="5.6" y2="45.8"/><line fill="none" x1="32.9" y1="34" x2="33.1" y2="45.8"/><path fill="none" d="M5.6,38.7"/><polygon points="20.6,52.8 15.9,52.8 15.4,52.4 20.2,52.4 "/></svg>',
  'card' : '<svg width="90%" height="90%" viewBox="0 0 45 28" enable-background="new 0 0 45 28" xml:space="preserve">  <path stroke="#000000" stroke-miterlimit="10" d="M41.5,27.5h-38c-1.6,0-3-1.4-3-3v-21c0-1.6,1.4-3,3-3h38    c1.7,0,3,1.4,3,3v21C44.5,26.1,43.2,27.5,41.5,27.5z"/>  <line x1="0.5" y1="7" x2="44.5" y2="7"/>  <line x1="0.5" y1="11.9" x2="44.5" y2="11.9"/>  <path d="M23.9,23.6H6c-1.7,0-3-1.4-3-3v-1.8c0-1.6,1.3-3,3-3h17.9 c1.7,0,3,1.4,3,3v1.8C26.9,22.3,25.6,23.6,23.9,23.6z"/>  <path d="M38.9,23.6h-6c-1.6,0-3-1.4-3-3v-1.8c0-1.6,1.4-3,3-3h6 c1.7,0,3,1.4,3,3v1.8C41.9,22.3,40.5,23.6,38.9,23.6z"/></svg>'
}
var zoom = d3.behavior.zoom()
    .translate([0, 0])
    .scale(1)
    .scaleExtent([1, 8])
    .on("zoom", zoomed);
var features = svg.append("g");
svg.append("rect")
    .attr("class", "overlay")
    .attr("width", width)
    .attr("height", height)
    .attr("x", -300)
    .attr("y", -300)
    .attr("fill", "rgba(0,0,0,0)")
    .call(zoom);
var circgroup = svg.append("g")
      .attr("class", "bubble");
var cur, curprop, breaker, rotateTimer;
d3.select('#center').on("click", function(){
  unzoom();
});
d3.json("us.json", function(error, us) {
  if (error) return console.error(error);
  var lines = [];
  for (var i = 0; i < us.objects.counties.geometries.length; i++) {
    // us.objects.counties.geometries[i].properties.topFT = {};
    us.objects.counties.geometries[i].properties.PCTspend = us.objects.counties.geometries[i].properties.spend_male / (us.objects.counties.geometries[i].properties.spend_female+us.objects.counties.geometries[i].properties.spend_male);
    us.objects.counties.geometries[i].properties.PCTcount = us.objects.counties.geometries[i].properties.count_male / (us.objects.counties.geometries[i].properties.count_female+us.objects.counties.geometries[i].properties.count_male);
  };
  d3.select("html").classed('loaded', true);
  var maxpers = d3.max(us.objects.counties.geometries, function(d) { return +d.properties.persons;} );
  var maxsm = d3.max(us.objects.counties.geometries, function(d) { return +d.properties.spend_male;} );
  var maxsf = d3.max(us.objects.counties.geometries, function(d) { return +d.properties.spend_female;} );
  var maxcm = d3.max(us.objects.counties.geometries, function(d) { return +d.properties.count_male;} );
  var maxcf = d3.max(us.objects.counties.geometries, function(d) { return +d.properties.count_female;} );
  var maxmp = d3.max(us.objects.counties.geometries, function(d) { 
    // d.properties.aveSpent = d.properties.spend_male/d.properties.count_male
    return d.properties.aveSpent;
  } );
  var minMW = d3.max(us.objects.counties.geometries, function(d) { 
    if(d.properties.count_female/(d.properties.count_female+d.properties.count_male) > 0.2){
      // console.log(d.properties.count_female/(d.properties.count_female+d.properties.count_male),d.properties.county,d.properties.state,d.properties.count_female);
    }
    return d.properties.count_female/(d.properties.count_female+d.properties.count_male);
  } );
  // console.log(minMW);
  var tfsm = getTop50( 'spend_male',us.objects.counties.geometries );
  var tfsf = getTop50( 'spend_female',us.objects.counties.geometries );
  var tfcf = getTop50( 'count_female',us.objects.counties.geometries );
  var tfcm = getTop50( 'count_male',us.objects.counties.geometries );

  var tfas = getTop50( 'aveSpent',us.objects.counties.geometries );

  var tfpc = getTop50( 'PCTspend',us.objects.counties.geometries );
  var tfpx = getTop50( 'PCTcount',us.objects.counties.geometries );
  var lineSettings = {
    'spend_male' : {'max':maxsm,'val':'tfsm'},
    'spend_female' : {'max':maxsf,'val':'tfsf'},
    'count_female' : {'max':maxcf,'val':'tfcf'},
    'count_male' : {'max':maxcm,'val':'tfcm'},
  }
  // listMaker(tfpc,'Male Spend Percent','PCTspend');
  // listMaker(tfpx,'Male Count Percent','PCTcount');
  var settings = [{'prop':'spend_male', 'fdom' : maxsf, 'mdom' : maxsm, 'sex' : 'male', 'ty' : 's', 'icon' : 'card', 'total' : 193884385.58, 'combined' : 196630110.05},
                  {'prop':'spend_female', 'fdom' : maxsf, 'mdom' : maxsm, 'sex' : 'female', 'ty' : 's', 'icon' : 'card', 'total' : 2745724.47, 'combined' : 196630110.05},
                  {'prop':'count_male', 'fdom' : maxcf, 'mdom' : maxcm, 'sex' : 'male', 'ty' : 'c', 'icon' : 'male', 'total' : 1006380, 'combined' : 1037968},
                  {'prop':'count_female', 'fdom' : maxcf, 'mdom' : maxcm, 'sex' : 'female', 'ty' : 'c', 'icon' : 'female', 'total' : 31588, 'combined' : 1037968}];
  var legends = [{'spend_male': maxsm,'spend_female': maxsf,'count_male': maxcm,'count_female': maxcf, 'persons': maxpers},
                  {'spend_male': maxsm/2,'spend_female': maxsf/2,'count_male': maxcm/2,'count_female': maxcf/2, 'persons': maxpers/2},
                  {'spend_male': maxsm/4,'spend_female': maxsf/4,'count_male': maxcm/4,'count_female': maxcf/4, 'persons': maxpers/4}];         
  var lists = d3.select(".lists").attr("class", "lists spend_male");
  var listCont = lists.append("div").attr("class", "uls").attr("id", "uls");
  var listTabs = lists.append("div").attr("class", "tabs");
  var lined = lists.append("div").attr("class", "lines");
  var clustered = lists.append("div").attr("class", "clustered");
  var tip = d3.tip()
    .attr('class', 'd3-tip map')
    .offset([0, 0])
    .html(function(d) {
      return "<div class='inform'><h4>"+d.properties.county + ", " + d.properties.state+"</h4><ul><li class='sm'><span>Men Spent</span><span>"+numword(d.properties.spend_male)+"</span></li><li class='sf'><span>Women Spent</span><span>"+numword(d.properties.spend_female)+"</span></li><li class='cm'><span>Men Members</span><span>"+numword(d.properties.count_male)+"</span></li><li class='cf'><span>Women Members</span><span>"+numword(d.properties.count_female)+"</span></li><li class='pc'><span>Male Count %</span><span>"+(d.properties.PCTcount*100).toFixed(2)+"%</span></li><li class='ps'><span>Male Spend %</span><span>"+(d.properties.PCTspend*100).toFixed(2)+"%</span></li><li><span>Ave. $ Spent</span><span>$"+(d.properties.spend_male/d.properties.count_male).toFixed(2)+"</span></li></ul></div>";
    });
  var untip = d3.tip()
    .attr('class', 'd3-tip map')
    .offset([0, 0])
    .html(function(d) {
      return "<div class='inform'><h4>Unrepresented Counties</h4><h4>"+unr+"</h4></div>";
    });
  var circtip = d3.tip()
    .attr('class', 'd3-tip lines')
    .offset([0, 0])
    .html(function(d) {
      return "<div id='ttl'><h4>"+d.place+"</h4><h4>"+numword(d.val)+"</h4></div>";
    });    
  var start = {'prop':'spend_male', 'max':maxsm};
  curprop = start.prop;
  listMaker(tfsm,'Males Spent','spend_male');
  listMaker(tfsf,'Females Spent','spend_female');
  listMaker(tfcf,'Female Count','count_female');
  listMaker(tfcm,'Male Count','count_male');
  listMaker(tfas,'Ave Spent','aveSpent');
  d3.select('.toggle').on('click', function(){
    if(d3.select(this).classed('male')){
      dom = 'fdom';
      d3.select(this).classed({
        'male' : false,
        'female' : true
      });
    }else{
      dom = 'mdom';
      d3.select(this).classed({
        'male' : true,
        'female' : false
      });
    }
    radius.domain([0, cur[dom]]);
    newCircRad();
    legendUpdate();
  });
  // d3.select('body').append('p').text(JSON.stringify(us));
  radius.domain([0, start.max]);

  svg.call(tip).call(untip);

  features.append("path")
      .datum(topojson.feature(us, us.objects.nation))
      .attr("class", "land")
      .attr("d", path);

  features.append("path")
      .datum(topojson.mesh(us, us.objects.states, function(a, b) { return a !== b; }))
      .attr("class", "border border--state")
      .attr("d", path);

  var menu = d3.select('nav').append('ul')
      .selectAll('li').data(settings)
      .enter();

  var lis = menu.append('li')
      .attr('class', function(d){ return d.prop + " " + d.sex; })
      .on('click', function(d){
        updateAll(d, this);
      });

  lis.append('div').attr('class', 'circ')
      .html(function(d){
        return imgs[d.icon];
      });

  lis.append('span')
      .text(function(d){
        if(d.ty === 's'){
          return spntxt;
        }else if(d.ty === 'c'){
          return memtxt;
        }
      });

  var circles = circgroup.selectAll("circle")
      .data(topojson.feature(us, us.objects.counties).features
      .sort(function(a, b) { return b.properties[start.prop] - a.properties[start.prop]; }))
      .enter().append("circle");

  var legend = features.append("g")
      .attr("class", "legend")
      .attr("transform", "translate(" + (width - 500) + "," + (height - 500) + ")")
      .selectAll("g")
      .data(legends)
      .enter().append("g");

  legend.append("circle");
  legend.append("text");

  circles.attr("transform", function(d) { return "translate(" + path.centroid(d) + ")"; })
      .attr("class", function(d){ return (d.properties.county + "" + d.properties.state).replace(" ", ""); })
      .attr("r", radius(0));
  circles.on("mouseover", tip.show).on("mouseout", tip.hide);

  newCircRad();
  legendUpdate();
  d3.select('li.spend_male').attr('class', function(d,i){
    d3.select(this).classed('active', true);
    d3.select('.uls ul.spend_male').classed("active", true);
    d3.select('.tabs div.spend_male').classed("active", true);
    transit(d);
    return d3.select(this).attr('class');
  });

  function transit(obj, sc){
    cur = obj;
    curprop = obj.prop;
    var sex = obj.sex;
    var ty = obj.ty;
    var tot = obj.total;
    var com = obj.combined;
    var to = 0;
    //variable counting counties not used
    //used if there is a need for a delay before transitioning
    if(sc){
      to = 500;
      circles.attr("r", function(d) { return radius(d.properties[curprop]); });
    }
    circles.classed('female male', false);
    circles.classed(sex, true);

    setTimeout(function(){
      radius.domain([0, obj[dom]]);
      circles.attr('class', function(d,i){
        if(d.properties[curprop]){
          return d3.select(this).attr("class").replace(" nact", "");
        }else{
          return d3.select(this).attr("class") + " nact";
        }
      })
      .on("mouseover", function(d){
        tip.show(d);
        meFun(d3.select(this).attr("class").split(" ")[0]);
      })
      .on("mouseout", function(d){
        tip.hide(d);
        leFun(d3.select(this).attr("class").split(" ")[0]);
      });
      d3.selectAll('.nact').on("mouseover", untip.show).on("mouseout", untip.hide);
      circles.transition().duration(1000).attr("transform", function(d) { 
        if(d.properties[curprop]){
          return "translate(" + path.centroid(d) + ")"; 
        }else{
          return "translate(-20,50)"; 
        }
      });
      newCircRad();
      legendUpdate();
      updateHeader();
      function updateHeader(){
        var dt = numword(tot);
        if(sex === 'male'){
          var men = ((tot / com) * 100).toFixed(2);
          var wom = (((com - tot) / com) * 100).toFixed(2);
        }else {
          var wom = ((tot / com) * 100).toFixed(2);
          var men = (((com - tot) / com) * 100).toFixed(2);            
        }
        if(ty === 'c'){
          d3.select('h1').text("There are "+dt+" paying "+sex+" members");
          d3.select('h5').text("Approximately "+men+"% of all paying members are male verses "+wom+"% of females");
        }else if(ty === 's'){
          d3.select('h1').text("Money spent by "+sex+" totaled $"+dt);
          d3.select('h5').text("Approximately "+men+"% of money spent was by males verses "+wom+"% of females");
        }    
      }
    }, to);  
  }
  function numword(num){
    var vald, valn;
    if(num > 1000000){
      vald = "Million"
      valn = 1000000;
      v = (num / valn).toFixed(2);
    }else if(num > 1000){
      vald = "Thousand"
      valn = 1000;
      v = (num / valn).toFixed(2);
    }else{
      vald = "";
      v = num;
    }
    return v + " " + vald;   
  }
  function legendUpdate(){
    legend.selectAll("circle").transition()
          .attr("cy", function(d) { return -radius(d[curprop]); })
          .attr('r', function(d){return radius(d[curprop]); });
    var ltt = legend.selectAll("text").transition()
          .attr("dy", function(d){ return -radius(d[curprop])*1.5; });
    ltt.text(function(d){
            return d3.format(".1s")(d[curprop]);
          });
  }
  function newCircRad(){ 
    unr = 0;
    circles
      .attr("class", function(d,i){
        if(d.properties.topFT[curprop]){
          return d3.select(this).attr("class") + " top50";
        }else{
          return d3.select(this).attr("class");
        }
      })
      .transition()
      .delay(function(d,i){
        return ((i % 50) * 25)+1000;
      })
      .duration(1000)
      .attr("r", function(d,i) {
        //for maximum value :: if(d.properties[prop] === dom)
        //for unused value :: if(d.properties[prop] < 1)
        if(d.properties[curprop]){
          // for adding values :: obj.total += d.properties[prop];
          return radius(d.properties[curprop]); 
        }else{
          unr += 1;
          return (1/unr) * 50;
        }
      });   
  }
  function getTop50( prop, arr ){
    arr.sort(function(x, y){
      return d3.descending(x.properties[prop], y.properties[prop]);
    });
    var newArr = arr.slice(0, 50);
    return newArr;
  }
  function listMaker(datas,label,prop){
    listCont.on('mouseenter',function(){rotater();}).on('mouseleave',function(){clearTimeout(rotateTimer);})
    listCont.append("ul").attr("class", prop).selectAll("li").data(datas.sort(function(a, b){
        return d3.descending(a.properties[prop], b.properties[prop]);
      })).enter().append('li').attr("class", function(d){
        return (d.properties.county + "" + d.properties.state).replace(" ", "");
      })
      .html(function(d,i){
        return '<div><span class="num">'+(i+1)+'</span><span class="place">'+d.properties.county+', '+d.properties.state+'</span><span class="prop">'+numword(d.properties[prop])+'</span></div>';
      })
      .on("mouseenter", function(){
        meFun(d3.select(this).attr("class").split(" ")[0]);
      })
      .on("mouseleave", function(){
        leFun(d3.select(this).attr("class").split(" ")[0]);
      });
    var ltd = listTabs.selectAll('.'+prop).data(settings.filter(function( obj ) {
                  return obj.prop == prop;
                })).enter().append('div').attr("class", prop);
    ltd.on("click", function(d){
      updateAll(d, this);
    });
    ltd.text(label);
  }
  lineGraph();
  function updateAll(d, that){
      var clss = d3.select(that).attr("class").replace(" active", "").replace(" female", "").replace(" male", "");
      lis.classed('active', false);
      d3.select('nav li.'+clss).classed('active', true);
      transit(d);
      var ulz = d3.select('.uls');
      ulz.selectAll("ul").classed("active", false);
      d3.select('.uls ul.'+clss).classed("active", true);
      d3.select('.lists').attr("class", "lists "+clss);
      d3.select('.tabs').selectAll("div").classed("active", false);
      d3.select('.tabs .'+clss).classed("active", true);
      curSwipe = 0;
      d3.select('#uls').selectAll('ul').style('margin-left', function(){
        return curSwipe + "%";
      });
      setTimeout(function(){
        updateLines(lineSettings[clss].val, lineSettings[clss].max, x, y);
      }, 1500);
  }
  function lineGraph(arr,prop){
    for (var i = 0; i < 50; i++) {
      lines.push({});
      lines[i].tfsm = {};
      lines[i].tfsf = {};
      lines[i].tfcf = {};
      lines[i].tfcm = {};

      lines[i].tfsm.val = tfsm[i].properties.spend_male;
      lines[i].tfsf.val = tfsf[i].properties.spend_female;
      lines[i].tfcf.val = tfcf[i].properties.count_female;
      lines[i].tfcm.val = tfcm[i].properties.count_male;

      lines[i].tfsm.place = tfsm[i].properties.county + ", " + tfsm[i].properties.state;
      lines[i].tfsf.place = tfsf[i].properties.county + ", " + tfsf[i].properties.state;
      lines[i].tfcf.place = tfcf[i].properties.county + ", " + tfcf[i].properties.state;
      lines[i].tfcm.place = tfcm[i].properties.county + ", " + tfcm[i].properties.state;
    };
    var linez = lined.append('div').attr('class', 'tg');
    linez.append('h6').attr('class', 'max');
    var top50 = linez.append('h4').attr('class', 'top50');
    top50.append('span').text('top');
    top50.append('span').text('50');
    linez.append('h6').attr('class', 'min');    

    var w = 1000;
    var h = 200;
    var lineSVG = lined.append("svg")
      .attr("id", "linez")
      .attr("width", "100%")
      .attr("height", "100%")
      .attr("viewBox", "-100 -50 "+w+" "+h);
    lineSVG.call(circtip);
    x = d3.scale.linear()
      .domain([0, 50])
      .range([0, w-200]);
    y = d3.scale.linear()
      .domain([800000, maxsm])
      .range([h-100,0]);
    var curd = "tfsm";
    var lineGroup = lineSVG.append('g').attr('class','lg').selectAll('line').data(lines).enter()
                        .append('line')
                        .attr('x1', function(d,i){
                          return x(i);
                        })
                        .attr('x2', function(d,i){
                          if(i < lines.length - 1){
                            return x(i+1);
                          }else{
                            return x(i);
                          }
                        });
    var circGroup = lineSVG.append('g').attr('class','cg').selectAll('circle').data(lines).enter()
                        .append('circle')
                        .attr('r', x(0.2))
                        .attr('cx', function(d,i){
                          return x(i);
                        });
    updateLines(curd, maxsm, x, y);
  }
  clusterGraph(tfsm,'spend_male');
  function clusterGraph(arr,prop){
    var width = 960,
        height = 500,
        padding = 1.5, // separation between same-color nodes
        maxRadius = 12;
  

    clustered
     var width = 960,
        height = 500,
        padding = 1.5, // separation between same-color nodes
        clusterPadding = 6, // separation between different-color nodes
        maxRadius = 12;

    var n = arr.length, // total number of nodes
        m = 1; // number of distinct clusters

    var color = d3.scale.category10()
        .domain(d3.range(m));

    // The largest node for each cluster.
    var clusters = new Array(m);

    var nodes = d3.range(n).map(function(d,j) {
      console.log(j)l
      var i = Math.floor(Math.random() * m),
          r = arr[j].properties[prop],
          d = {cluster: i, radius: r};
      if (!clusters[i] || (r > clusters[i].radius)) clusters[i] = d;
      return d;
    });

    // Use the pack layout to initialize node positions.
    d3.layout.pack()
        .sort(null)
        .size([width, height])
        .children(function(d) { return d.values; })
        .value(function(d) { return d.radius * d.radius; })
        .nodes({values: d3.nest()
          .key(function(d) { return d.cluster; })
          .entries(nodes)});

    var force = d3.layout.force()
        .nodes(nodes)
        .size([width, height])
        .gravity(.02)
        .charge(0)
        .on("tick", tick)
        .start();

    var svg = clustered.append("svg")
        .attr("width", width)
        .attr("height", height);

    var node = svg.selectAll("circle")
        .data(nodes)
      .enter().append("circle")
        .style("fill", function(d) { return color(d.cluster); })
        .call(force.drag);

    node.transition()
        .duration(750)
        .delay(function(d, i) { return i * 5; })
        .attrTween("r", function(d) {
          var i = d3.interpolate(0, d.radius);
          return function(t) { return d.radius = i(t); };
        });

    function tick(e) {
      node
          .each(cluster(10 * e.alpha * e.alpha))
          .each(collide(.5))
          .attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });
    }

    // Move d to be adjacent to the cluster node.
    function cluster(alpha) {
      return function(d) {
        var cluster = clusters[d.cluster];
        if (cluster === d) return;
        var x = d.x - cluster.x,
            y = d.y - cluster.y,
            l = Math.sqrt(x * x + y * y),
            r = d.radius + cluster.radius;
        if (l != r) {
          l = (l - r) / l * alpha;
          d.x -= x *= l;
          d.y -= y *= l;
          cluster.x += x;
          cluster.y += y;
        }
      };
    }

    // Resolves collisions between d and all other circles.
    function collide(alpha) {
      var quadtree = d3.geom.quadtree(nodes);
      return function(d) {
        var r = d.radius + maxRadius + Math.max(padding, clusterPadding),
            nx1 = d.x - r,
            nx2 = d.x + r,
            ny1 = d.y - r,
            ny2 = d.y + r;
        quadtree.visit(function(quad, x1, y1, x2, y2) {
          if (quad.point && (quad.point !== d)) {
            var x = d.x - quad.point.x,
                y = d.y - quad.point.y,
                l = Math.sqrt(x * x + y * y),
                r = d.radius + quad.point.radius + (d.cluster === quad.point.cluster ? padding : clusterPadding);
            if (l < r) {
              l = (l - r) / l * alpha;
              d.x -= x *= l;
              d.y -= y *= l;
              quad.point.x += x;
              quad.point.y += y;
            }
          }
          return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
        });
      };
    }

  }
  function updateLines(curd,sc,x,y){
    var scm = d3.min(lines, function(d) { return d[curd].val; });
    y.domain([scm, sc]);
    var dlg = d3.select('.lines .lg')
      .selectAll('line')
      .transition()
      .attr('y1', function(d,i){
        return y(d[curd].val);
      })
      .attr('y2', function(d,i){
        if(i < lines.length - 1){
          return y(lines[i+1][curd].val);
        }else{
          return y(d[curd].val);
        }
      });
    var dcg = d3.select('.lines .cg')
      .selectAll('circle')
      .attr('class', function(d,i){
        return d[curd].place.split(" ").join("").replace(",", "");
      });
    dcg.transition().attr('cy', function(d,i){
        return y(d[curd].val);
      });
    dcg.on("mouseenter", function(d){
        meFun(d3.select(this).attr("class").split(" ")[0]);
        circtip.show(d[curd]);
      })
      .on("mouseleave", function(d){
        leFun(d3.select(this).attr("class").split(" ")[0]);
        circtip.hide(d[curd]);
      });
    d3.select('.tg h6.min').text('min : '+numword(scm));
    d3.select('.tg h6.max').text('max : '+numword(sc));

  }
  d3.select(window).on("scroll", function(){
    sy = this.top.scrollY;
    // console.log(breaker, d3.select(".d3-tip"));
    // console.log(sy);
    // d3.select(".d3-tip.lines").style("margin-top", -(sy/3)+'px');
    // d3.selectAll(".d3-tip.map").style("margin-top", (sy/3)+'px');
    if(sy > breaker && !d3.select('.map').classed('small')){
      d3.select('.map').classed('small', true);
    }else if(sy < breaker && d3.select('.map').classed('small')){
      d3.select('.map').classed('small', false);      
    }
  })
  .on("resize", function(){
    chooseResize();
  });
  chooseResize();
  function chooseResize(){
    if(this.innerWidth > 1024 && breaker !== 200){
      breaker = 200;
    }else if(this.innerWidth < 1024 && breaker !== 200){
      breaker = 200;
    }
  }
  function meFun(cls){
    d3.selectAll('#uls li.'+cls).classed('hover', true);
    d3.select('#linez circle.'+cls).transition().attr('r', x(0.5));
    d3.select('.map .bubble circle.'+cls).classed('hover', true);
    d3.select('.map .bubble circle.'+cls).transition().attr("r",function(d){
      return radius(d.properties[curprop])*1.2;
    });
  }
  function leFun(cls){
    d3.selectAll('#uls li.'+cls).classed('hover', false);
    d3.select('#linez circle.'+cls).transition().attr('r', x(0.2));
    d3.select('.map .bubble circle.'+cls).classed('hover', false);
    d3.select('.map .bubble circle.'+cls).transition().attr("r",function(d){
      return radius(d.properties[curprop]);
    });
  }
  function rotater(){
    swipeLeft(5);
    rotateRepeat();
  }
  function rotateRepeat(){
    rotateTimer = setTimeout(function(){
      swipeLeft(15);
      rotateRepeat();
    }, 2000);    
  }
  function swipeLeft(amt){
      if(curSwipe < -401){
        curSwipe = 0
      }else{
        curSwipe -= amt;
      }
      d3.select('#uls').selectAll('ul').style('margin-left', function(){
        return curSwipe + "%";
      });
  }
  function swipeRight(amt){
      if(curSwipe > 0){
        curSwipe = 0
      }else{
        curSwipe += amt;
      }
      d3.select('#uls').selectAll('ul').style('margin-left', function(){
        return curSwipe + "%";
      });
  }
  hammerFunctions();
  function hammerFunctions(){
    var myElement = document.getElementById('uls');
    var hammertime = new Hammer(myElement);
    hammertime.on('swipeleft', function(ev) {
      clearTimeout(rotateTimer);
      swipeLeft(40);
    });
    hammertime.on('swiperight', function(ev) {
      clearTimeout(rotateTimer);
      swipeRight(40);
    });  
  }
});
function unzoom() {   
  features.transition().attr("transform", "translate(0,0)scale(1)");
  circgroup.transition().attr("transform", "translate(0,0)scale(1)");
}
function zoomed() {
  var det = d3.event.translate;
  var des = d3.event.scale;
  // if(det[0] > (width*des)/2){
  //   det[0] = (width*des)/2;
  // }else if(det[0] < -(width*des)/2){
  //   det[0] = -(width*des)/2;
  // }
  // if(det[1] > (height*des)/2){
  //   det[1] = (height*des)/2;
  // }else if(det[1] < -(height*des)/2){
  //   det[1] = -(height*des)/2;
  // }    
  features.transition().attr("transform", "translate(" + det + ")scale(" + des + ")");
  circgroup.transition().attr("transform", "translate(" + det + ")scale(" + des + ")");
}  